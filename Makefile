executables=target/main-v1 target/main-v1-opt target/main-v2

.PHONY: run
run: $(executables)
	$(foreach exe, $^, $(exe);)

.PHONY: all
all: $(executables)

target/main-v1: v1/main.cc v1/element_base.cc v1/element1.cc v1/element2.cc

target/main-v1-opt: FLAGS=-O3
target/main-v1-opt: v1/main.cc v1/element_base.cc v1/element1.cc v1/element2.cc

target/main-v2: v2/main.cc v2/element_base.cc v2/element1.cc v2/element2.cc

$(executables): | target
	@g++ --version
	g++ $^ -o $@ -pedantic -std=c++20 -Wall -Wextra -Wcast-align $(FLAGS)
	@echo "BINARY DUMP for $@ [[["
	@strings $@ | grep -e "name-element" -e "s-element"
	@echo "]]]"

.PHONY: clean
clean:
	rm -f core.*
	rm -f $(executables)

target:
	@mkdir -p target
