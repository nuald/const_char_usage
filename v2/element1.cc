#include <cstring>
#include "element1.hh"

// tag::definition[]
const char* const Element1::kClassName = "name-element1";

using namespace std::string_literals;
const std::string Element1::sClassName = "s-element1"s;
// end::definition[]

bool Element1::is_same_type(const char* name) const {
  return !strcmp(name, kClassName);
}

bool Element1::is_same_type(const std::string& name) const {
  return name == sClassName;
}
