#include <cstring>
#include "element_base.hh"

using namespace std::string_literals;

const char* const ElementBase::kClassName = "name-element-base";
const std::string ElementBase::sClassName = "s-element-base"s;

bool ElementBase::is_same_type(const char* name) const {
  return !strcmp(name, kClassName);
}

bool ElementBase::is_same_type(const std::string& name) const {
  return name == sClassName;
}
