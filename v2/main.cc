#include <iostream>
#include <memory>
#include <sstream>
#include <unistd.h>
#include "element_base.hh"
#include "element1.hh"
#include "element2.hh"

// tag::functions[]
template<typename T> void print_class_name() {
  std::cout << T::kClassName << std::endl;
}

// C++20, w/constrain for types derived from ElementBase only
template<std::derived_from<ElementBase> T>
void print_class_name_modern() {
  std::cout << T::sClassName << std::endl;
}
// end::functions[]

int main([[maybe_unused]] int argc, char *argv[]) {
  // tag::usage[]
  std::cout << Element2::kClassName << std::endl;
  std::cout << Element2::sClassName << std::endl;
  print_class_name<Element2>();
  print_class_name_modern<Element2>();
  // end::usage[]

  const std::unique_ptr<Element> elems[] = {
    std::make_unique<ElementBase>(),
    std::make_unique<Element2>(),
    std::make_unique<Element1>(),
    std::make_unique<ElementBase>(),
    std::make_unique<Element2>(),
    std::make_unique<Element2>(),
    std::make_unique<ElementBase>(),
    std::make_unique<Element1>()
  };

  for (const auto & el: elems) {
    std::cout << el->class_name() << " "
      << el->is_same_type(ElementBase::kClassName) << " "
      << el->is_same_type(Element1::kClassName) << " "
      << el->is_same_type(Element2::kClassName) << " "
      << el->is_same_type(ElementBase::sClassName) << " "
      << el->is_same_type(Element1::sClassName) << " "
      << el->is_same_type(Element2::sClassName) << " "
      << std::endl;
  }

  auto pid = getpid();
  std::stringstream cmd;
  cmd << "gcore " << pid << " && strings core." << pid
    << " | grep -e name-element -e s-element";
  std::cout << "MEMORY DUMP for " << argv[0] << " [[[" << std::endl;
  system(cmd.str().c_str());
  std::cout << "]]]" << std::endl;

  return EXIT_SUCCESS;
}
