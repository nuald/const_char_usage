#include <cstring>
#include "element2.hh"

using namespace std::string_literals;

const char* const Element2::kClassName = "name-element2";
const std::string Element2::sClassName = "s-element2"s;

bool Element2::is_same_type(const char* name) const {
  return !strcmp(name, kClassName);
}

bool Element2::is_same_type(const std::string& name) const {
  return name == sClassName;
}
