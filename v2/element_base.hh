#pragma once
#include "element.hh"

struct ElementBase: Element {
  static const char* const kClassName;
  static const std::string sClassName;

  const char* class_name() const override { return kClassName; }
  std::string class_name_s() const override { return sClassName; }

  bool is_same_type(const char* name) const override;
  bool is_same_type(const std::string& name) const override;
};
