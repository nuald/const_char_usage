#pragma once
#include "element_base.hh"

struct Element2: ElementBase {
  const char* class_name() const override { return "name-element2"; }

  std::string class_name_s() const override {
    using namespace std::string_literals;
    return "s-element2"s;
  }

  bool is_same_type(const char* name) const override;
  bool is_same_type(const std::string& name) const override;
};
