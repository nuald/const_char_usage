#pragma once
#include <string>

struct Element {
  virtual const char* class_name() const = 0;
  virtual std::string class_name_s() const = 0;
  virtual bool is_same_type(const char* name) const = 0;
  virtual bool is_same_type(const std::string& name) const = 0;
};
