#include <cstring>
#include "element_base.hh"

bool ElementBase::is_same_type(const char* name) const {
  return !strcmp(name, "name-element-base");
}

bool ElementBase::is_same_type(const std::string& name) const {
  using namespace std::string_literals;
  return name == "s-element-base"s;
}
