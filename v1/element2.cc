#include <cstring>
#include "element2.hh"

bool Element2::is_same_type(const char* name) const {
  return !strcmp(name, "name-element2");
}

bool Element2::is_same_type(const std::string& name) const {
  using namespace std::string_literals;
  return name == "s-element2"s;
}
