#include <iostream>
#include <memory>
#include <sstream>
#include <unistd.h>
#include "element_base.hh"
#include "element1.hh"
#include "element2.hh"

int main([[maybe_unused]] int argc, char *argv[]) {
  using namespace std::string_literals;

  const std::unique_ptr<Element> elems[] = {
    std::make_unique<ElementBase>(),
    std::make_unique<Element2>(),
    std::make_unique<Element1>(),
    std::make_unique<ElementBase>(),
    std::make_unique<Element2>(),
    std::make_unique<Element2>(),
    std::make_unique<ElementBase>(),
    std::make_unique<Element1>()
  };

  for (const auto & el: elems) {
    std::cout << el->class_name() << " "
      << el->is_same_type("name-element-base") << " "
      << el->is_same_type("name-element1") << " "
      << el->is_same_type("name-element2") << " "
      << el->is_same_type("s-element-base"s) << " "
      << el->is_same_type("s-element1"s) << " "
      << el->is_same_type("s-element2"s) << " "
      << std::endl;
  }

  auto pid = getpid();
  std::stringstream cmd;
  cmd << "gcore -a " << pid << " && strings core." << pid
    << " | grep -e name-element -e s-element";
  std::cout << "MEMORY DUMP for " << argv[0] << " [[[" << std::endl;
  system(cmd.str().c_str());
  std::cout << "]]]" << std::endl;

  return EXIT_SUCCESS;
}
