#pragma once
#include "element.hh"

struct ElementBase: Element {
  const char* class_name() const override { return "name-element-base"; }

  std::string class_name_s() const override {
    using namespace std::string_literals;
    return "s-element-base"s;
  }

  bool is_same_type(const char* name) const override;
  bool is_same_type(const std::string& name) const override;
};
