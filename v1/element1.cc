#include <cstring>
#include "element1.hh"

bool Element1::is_same_type(const char* name) const {
  return !strcmp(name, "name-element1");
}

bool Element1::is_same_type(const std::string& name) const {
  using namespace std::string_literals;
  return name == "s-element1"s;
}
